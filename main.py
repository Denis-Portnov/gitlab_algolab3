class PriorityQueue:

    def __init__(self):
        self.queue = [0] * 1000000
        self.HeapSize = 0

    def push(self, x):
        self.queue[self.HeapSize] = x
        self.HeapSize += 1
        self.siftUp(self.HeapSize - 1)

    def siftUp(self, i):
        if i > 0 and self.queue[i] < self.queue[(i - 1) // 2]:
            self.queue[i], self.queue[(i - 1) // 2] = self.queue[(i - 1) // 2], self.queue[i]
            self.siftUp((i - 1) // 2)

    def extractMin(self):
        if self.HeapSize > 0:
            returnValue = self.queue[0]
            self.HeapSize -= 1
            self.queue[0] = self.queue[self.HeapSize]
            self.siftDown(0)
            return returnValue
        else:
            return '*'

    def siftDown(self, i):
        while 2 * i + 1 < self.HeapSize:
            l = 2 * i + 1
            r = 2 * i + 2
            j = l
            if r < self.HeapSize and self.queue[j] > self.queue[r]:
                j = r
            if self.queue[j] >= self.queue[i]:
                break
            self.queue[i], self.queue[j] = self.queue[j], self.queue[i]
            i = j

    def descreaseKey(self, oldKey, newKey):
        index = self.findByKey(oldKey)
        self.queue[index] = newKey
        self.siftUp(index)

    def findByKey(self, key):
        for i in range(self.HeapSize):
            if self.queue[i] == key:
                return i
        return -1


fin = open('priorityqueue.in')
fout = open('priorityqueue.out', 'w')
prioritity_queue = PriorityQueue()
keys = [0] * 1000000
counter = 0
overall = fin.readlines()
n = len(overall)
for i in range(n):
    s = overall[i]
    if 'push' in s:
        com, x = s.split()
        x = int(x)
        prioritity_queue.push(x)
        keys[counter] = x
    elif 'extract-min' in s:
        min = prioritity_queue.extractMin()
        print(min, file=fout)
    elif 'decrease-key' in s:
        com, x, y = s.split()
        x = int(x)
        y = int(y)
        prioritity_queue.descreaseKey(keys[x - 1], y)
        keys[x - 1] = y
    counter+=1
fout.close()
